#include "protheus.ch"
#include "restful.ch"
#include "fwmvcdef.ch"

// fun��o dummy para listagem no rpo
user function todorest()
return

WSRESTFUL todo DESCRIPTION "todo" FORMAT "application/json"
	WSDATA cCode AS STRING
    WSDATA cContent AS STRING
    WSDATA dDate AS DATE

	WSMETHOD GET todos DESCRIPTION "todos" PATH "" PRODUCES APPLICATION_JSON
    WSMETHOD GET todo DESCRIPTION "todo" PATH "{cCode}" PRODUCES APPLICATION_JSON
    WSMETHOD POST todo DESCRIPTION "todo" PATH "" PRODUCES APPLICATION_JSON
    WSMETHOD PUT todo DESCRIPTION "todo" PATH "{cCode}" PRODUCES APPLICATION_JSON
    WSMETHOD DELETE todo DESCRIPTION "todo" PATH "{cCode}" PRODUCES APPLICATION_JSON

END WSRESTFUL

WSMETHOD GET todos WSREST todo

    Local cSZ0Tab := 'SZ0'
    Local cFilUSe := '01'

    varinfo( 'info:', self )

    //RpcSetEnv('99',cFilUSe,,'TEC')

    // abre a estrutura de retorno
    Self:SetResponse('[')

    DbSelectArea('SZ0')
    (cSZ0Tab)->( DbSetOrder( 1 ) ) // Z0_FILIAL+Z0_CODE
    (cSZ0Tab)->( DbSeek( xFilial('SZ0', cFilUSe) ) )

    While (cSZ0Tab)->(!EOF())

        // preenche o retorno com os dados
        Self:SetResponse( todoJsOne( cSZ0Tab ) )

        (cSZ0Tab)->(DbSkip())
        
        If (cSZ0Tab)->(!EOF())
            Self:SetResponse(',')
        EndIf
    End

    (cSZ0Tab)->(DbCloseArea())
    // fecha a estrutura de retorno
    Self:SetResponse(']')
    
    //RpcClearEnv()
Return .T.

WSMETHOD GET todo WSRECEIVE cCode WSREST todo
    Local lRet := .T.
    Local cSZ0Tab := 'SZ0'
    Local cFilUSe := '01'

    If Len(self:aURLParms) > 0
        cCode := self:aURLParms[1]

        // RpcSetEnv('99',cFilUSe,,'TEC')

        DbSelectArea('SZ0')
        (cSZ0Tab)->( DbSetOrder( 1 ) ) // Z0_FILIAL+Z0_CODE
        
        If (cSZ0Tab)->( DbSeek( xFilial('SZ0', cFilUSe) + cCode ) )

            // abre a estrutura de retorno
            //Self:SetResponse('{ "todo":')

                // preenche o retorno com os dados
                Self:SetResponse( todoJsOne( cSZ0Tab ) )
            
            // fecha a estrutura de retorno
            //Self:SetResponse('}')
        Else
            lRet := .F.
            SetRestFault(404, 'recurso n�o encontrado')            
        EndIf

        // RpcClearEnv()
    Else
        lRet := .F.
        SetRestFault(400, 'parametro id � obrigat�rio')
    EndIf

Return lRet


WSMETHOD POST todo WSRECEIVE cCode WSREST todo
    Local lPost := .T.
    Local cBody := self:GetContent()
    Local oJson := Nil
    Local oMdl  := FwLoadModel( "TODOMVC" )
    Local cContent := ""
    Local dDate := CTOD("")

    FWJsonDeserialize( cBody, @oJson )
    
    oMdl:SetOperation( MODEL_OPERATION_INSERT )
    lPost := oMdl:Activate()

    cContent := oJson:description
    lPost := lPost .And. oMdl:GetModel("SZ0MASTER"):SetValue("Z0_CONTENT", cContent)
    
    dDate := STOD( StrTran( oJson:creationDate, "-", "" ) )
    lPost := lPost .And. oMdl:GetModel("SZ0MASTER"):SetValue("Z0_DATE", dDate)
    
    // grava os dados
    lPost := lPost .And. oMdl:VldData() .And. oMdl:CommitData()

    If lPost
        //self:SetResponse( '{"todo":'+ todoJsOne( "SZ0" ) +'}' )
        self:SetResponse( todoJsOne( "SZ0" ) )
    Else
        SetRestFault( 400, oMdl:GetErrorMessage()[MODEL_MSGERR_MESSAGE] )
    EndIf

Return lPost


WSMETHOD PUT todo WSRECEIVE cCode WSREST todo
    Local lProcessed := .T.
    Local cBody := self:GetContent()
    Local aParams := self:aURLParms
    Local cFilUSe := "01"
    Local cSZ0Tab := 'SZ0'
    Local oMdl  := Nil
    Local oJson := Nil
    Local cContent := ""
    Local dDate := CTOD("")

    FWJsonDeserialize( cBody, @oJson )

    If Len(aParams) > 0
        cCode := aParams[1]

        //RpcSetEnv('99',cFilUSe,,'TEC')

        DbSelectArea('SZ0')
        (cSZ0Tab)->( DbSetOrder( 1 ) ) // Z0_FILIAL+Z0_CODE
        
        If (cSZ0Tab)->( DbSeek( xFilial('SZ0', cFilUSe) + cCode ) )
            oMdl := FwLoadModel( "TODOMVC" )
        
            oMdl:SetOperation( MODEL_OPERATION_UPDATE )
            lProcessed := oMdl:Activate()

            //Altera o conte�do
            cContent := oJson:description
            lProcessed := lProcessed .And. oMdl:GetModel("SZ0MASTER"):SetValue("Z0_CONTENT", cContent)

            //Altera a data
            dDate := STOD( StrTran( oJson:creationDate, '-', '' ) )
            lProcessed := lProcessed .And. oMdl:GetModel("SZ0MASTER"):SetValue("Z0_DATE", dDate)

            // grava os dados
            lProcessed := lProcessed .And. oMdl:VldData() .And. oMdl:CommitData()

            If lProcessed
                // abre a estrutura de retorno
                //Self:SetResponse('{ "todo": ')

                Self:SetResponse( todoJsOne("SZ0") )
                
                // fecha a estrutura de retorno
                //Self:SetResponse('}')
            Else
                SetRestFault( 400, 'conte�do inv�lido para atualiza��o')
            EndIf
        Else
            lProcessed := .F.
            SetRestFault( 404, 'c�digo '+ cCode +' n�o encontrado' )
        EndIf
        //RpcClearEnv()
    Else
        lProcessed := .F.
        SetRestFault( 400, 'parametro id � obrigat�rio' )
    EndIf
Return lProcessed


WSMETHOD DELETE todo WSRECEIVE cCode WSREST todo
    Local lProcessed := .T.
    Local aParams := self:aURLParms
    Local cFilUSe := "01"
    Local cSZ0Tab := 'SZ0'
    Local oMdl  := Nil

    If Len(aParams) > 0
        cCode := aParams[1]

        //RpcSetEnv('99',cFilUSe,,'TEC')

        DbSelectArea('SZ0')
        (cSZ0Tab)->( DbSetOrder( 1 ) ) // Z0_FILIAL+Z0_CODE
        
        If (cSZ0Tab)->( DbSeek( xFilial('SZ0', cFilUSe) + cCode ) )

            oMdl := FwLoadModel( "TODOMVC" )
        
            oMdl:SetOperation( MODEL_OPERATION_DELETE )
            lProcessed := oMdl:Activate()

            // grava os dados
            lProcessed := lProcessed .And. oMdl:VldData() .And. oMdl:CommitData()

            If lProcessed
                // abre e fecha a estrutura de retorno
                Self:SetResponse('{}')
            Else
                SetRestFault( 400, 'problemas na exclus�o - ' + oMdl:GetErrorMessage()[MODEL_MSGERR_MESSAGE] )
            EndIf
            
        Else
            lProcessed := .F.
            SetRestFault( 404, 'c�digo '+ cCode +' n�o encontrado' )
        EndIf
        //RpcClearEnv()
    Else
        lProcessed := .F.
        SetRestFault( 400, 'parametro id � obrigat�rio' )
    EndIf
Return lProcessed


Static Function todoJsOne( cSZ0Tab )
Local cRet := ""
Local cCreationDate := ""

Default cSZ0Tab := "SZ0"

cCreationDate := Transform( DTOS((cSZ0Tab)->Z0_DATE ), "@R 9999-99-99" )

cRet := EncodeUTF8('{"id":"'+(cSZ0Tab)->Z0_CODE+'","description":"'+(cSZ0Tab)->Z0_CONTENT+'","creationDate":"'+cCreationDate+'"}')
cRet := StrTran( cRet, CHR(10), "" )
cRet := StrTran( cRet, CHR(13), "" )

Return cRet
