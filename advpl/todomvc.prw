#include "protheus.ch"
#include "fwmvcdef.ch" 

User Function todomvc()

Local oBrowse := FwMBrowse():New()

oBrowse:SetAlias( "SZ0" )
oBrowse:SetDescription( "Lista de to-do's" )
oBrowse:Activate()

Return 

Static Function Menudef()

Local aRotina := {}

ADD OPTION aRotina TITLE "Pesquisar" ACTION "PesqBrw"         OPERATION 1 ACCESS 0
ADD OPTION aRotina TITLE "Visualizar" ACTION "VIEWDEF.TODOMVC" OPERATION 2 ACCESS 0
ADD OPTION aRotina TITLE "Incluir" ACTION "VIEWDEF.TODOMVC" OPERATION 3 ACCESS 0
ADD OPTION aRotina TITLE "Alterar" ACTION "VIEWDEF.TODOMVC" OPERATION 4 ACCESS 0
ADD OPTION aRotina TITLE "Excluir" ACTION "VIEWDEF.TODOMVC" OPERATION 5 ACCESS 0

Return aRotina

Static Function ModelDef()
Local oModel := Nil
Local oStruSZ0 := FWFormStruct(1,'SZ0')

oModel := MpFormModel():New("TODO")
oModel:AddFields( "SZ0MASTER", , oStruSZ0 )
oModel:SetPrimaryKey( { "Z0_FILIAL", "Z0_CODE" } )
oModel:GetModel( "SZ0MASTER" ):SetDescription("TO-DO")
oModel:SetDescription("TO-DO")

Return oModel

Static Function Viewdef()
Local oView := Nil
Local oStruSZ0 := FWFormStruct(2,'SZ0')
Local oModel := FwLoadModel("TODOMVC")

oView := FWFormView():New()
oView:SetModel( oModel )
oView:AddField( 'MASTER', oStruSZ0, 'SZ0MASTER' ) 
oView:CreateHorizontalBox( 'TELA', 100)
oView:SetOwnerView( 'MASTER','TELA')
oView:EnableTitleView( 'MASTER', "TO-DO" )

Return oView