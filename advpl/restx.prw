#INCLUDE 'protheus.CH'
#INCLUDE "RESTFUL.CH"

// **** NEW DUMMY ****
WSRESTFUL dummy2 DESCRIPTION "dummy2" FORMAT "application/json"
	WSDATA apiVersion AS CHARACTER
	WSDATA nI AS STRING OPTIONAL

	WSMETHOD GET root DESCRIPTION "Get root" PATH "{apiVersion}" PRODUCES APPLICATION_JSON
	WSMETHOD GET oi DESCRIPTION "Get oi" PATH "{apiVersion}/oi" PRODUCES APPLICATION_JSON
	WSMETHOD POST oi DESCRIPTION "POST oi" PATH "{apiVersion}/oi" PRODUCES APPLICATION_JSON
END WSRESTFUL

WSMETHOD GET root WSRECEIVE nI WSREST dummy2
	Self:SetResponse('[{"version":"'+self:apiVersion+'","msg":"root","nI":"'+cValToChar(self:nI)+'"}]')
Return .T.

WSMETHOD GET oi WSREST dummy2
	Self:SetResponse('[{"version":"'+self:apiVersion+'","msg":"oi","nI":"'+cValToChar(self:nI)+'"}]')
Return .T.

WSMETHOD POST oi WSREST dummy2
	Self:SetResponse('[{"version":"'+self:apiVersion+'","msg":"'+self:GetContent()+'","nI":"'+cValToChar(self:nI)+'"}]')
Return .T.