import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ViewController } from 'ionic-angular';
import { Task } from './../../models/task.model';
import * as moment from 'moment';
import { ClientAdvplProvider } from './../../providers/client-advpl/client-advpl'

/**
 * Generated class for the AddPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-add',
  templateUrl: 'add.html',
})
export class AddPage {

  private task: Task = new Task();

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, 
      public alertCtrl: AlertController, public wsAdvpl: ClientAdvplProvider ) {
    
    if (navParams.get('task')){
      this.task = navParams.get('task') as Task;
    } else {
      this.task = new Task();
    }
  }

  saveTask(){
    // descobrir pq precisa adicionar 5h para encontrar a data informada
    this.task.creationDate = new Date( this.task.formattedDate + ' 05:00:00.000' );
    this.task.displayDate = moment(this.task.creationDate).format('DD/MM/YYYY');
    this.task.formattedDate = moment(this.task.creationDate).format('YYYY-MM-DD');
    // chama rest para salvar as informações
    this.wsAdvpl.saveTask( this.task ).subscribe(
      (res) => {
        this.showAlert('Item inserido com sucesso!');
      },
      (err) => {
        this.showErrorAlert( err );
      }
    );
  }

  deleteTask(){
    this.showConfirm();
  }

  showConfirm(){
    var that = this;
    let confirm = this.alertCtrl.create({
      title: 'Exclusão',
      message: 'Tem certeza que deseja excluir?',
      buttons: [
        {
          text: 'Não',
          handler: () => {
            // quando escolhido não, volta normalmente
          }
        },
        {
          text: 'Sim',
          handler: () => {
            // realiza a exclusão do item
            that.wsAdvpl.deleteTask( that.task.id ).subscribe(
              (res) => {
                that.showAlert('Excluído com sucesso!');
              },
              (err) => {
                that.showErrorAlert(err);
              }
            )
          }
        }
      ]
    });
    confirm.present();
  }

  showAlert( msg ){
    let alert = this.alertCtrl.create({
      title: 'Sucesso!',
      subTitle: msg,
      buttons: ['OK']
    });
    alert.present().then(
      (res) => {
        this.viewCtrl.dismiss();
      }
    )
  }

  showErrorAlert( msg ){
    let alert = this.alertCtrl.create({
      title: 'Erro',
      subTitle: 'Erro: ' + msg,
      buttons: ['OK']
    });
    alert.present();
  }

}
