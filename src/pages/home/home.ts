import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
import { Task } from './../../models/task.model';
import { AddPage } from './../add/add';
import { ClientAdvplProvider } from './../../providers/client-advpl/client-advpl'

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  tasks: Task[] = [];

  constructor(public navCtrl: NavController, public navParam: NavParams, private toastCtrl: ToastController,
               public wsAdvpl: ClientAdvplProvider ) {

  }
  
  ionViewDidLoad() {
  }

  ionViewDidEnter() {
    this.readTasks();
  }

  addTask(){
    this.navCtrl.push(AddPage, {});
  }

  itemTapped( event, task ){
    this.navCtrl.push(AddPage, {
      task: task
    });
  }

  readTasks(){
    //let newTask: Task;

    // for( let x: number = 1; x <= 10; x++ ) {
    //   newTask = new Task();
    //   newTask.id = x.toString();
    //   newTask.description = `a hard code task ${x}\n\n\n\n last line`;
    //   newTask.creationDate = new Date( '2017-08-'+ x.toString() );

    //   this.tasks.push( Task.format( newTask ) );
    // }

    this.wsAdvpl.getTasks().subscribe(
      (res)=>{
        //console.log('Res: ', res);
        this.tasks = res;
      }
    );
  }

  insertTask( task: Task ){
    this.tasks.push( task );
  }
}
