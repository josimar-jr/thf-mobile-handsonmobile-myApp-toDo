import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Task } from './../../models/task.model';

/*
  Generated class for the ClientAdvplProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class ClientAdvplProvider {

  private url: string = 'http://localhost:8084/rest/todo/';
  public retorno: any;
  public headers = new Headers({ 'Content-Type': 'application/json', 'TenantId': '99,01' });

  constructor(public http: Http) {
      
  }

  public getTasks() : Observable<Task[]> {
    return this.http.get(this.url)
      .map( res => res.json() )
      .map(
        (tasks) => {
          let newTasks : Task[] = [];
          tasks.forEach(
            element => {
              newTasks.push( Task.format(element) );
          }
        );
       return newTasks;
      });
  }

  public saveTask( task: Task ){
    if( task.id ) {
      return this.http.put( this.url + '/' + task.id, task );
    } else {
      return this.http.post( this.url, task );
    }
  }

  public deleteTask( id: string ){
    return this.http.delete( this.url + '/' + id );
  }

}
