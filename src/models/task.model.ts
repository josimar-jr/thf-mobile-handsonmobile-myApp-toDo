import * as moment from 'moment';

export class Task{
	id: string;
	description: string;
	creationDate: Date;
	displayDate: string;
	formattedDate: string;

	static format( t: any): Task {
		let task = new Task();
		
		task.id = t.id;
		task.description = t.description;
		task.creationDate = new Date(t.creationDate);
		task.displayDate = moment(t.creationDate).format('DD/MM/YYYY');
		task.formattedDate = moment(t.creationDate).format('YYYY-MM-DD');
		
		return task;
	}
}