### THF-MOBILE Sample App - Hands On 


Este aplicativo foi criado a partir do Hands On ministrado há algum tempo atrás.

O único dia reservado por aquele Hands On não foi suficiente para que todos pudessem conseguir produzir algo usando o thf-mobile nem o rest do protheus devidamente.

Então eu resolvi continuar a ideia original do Hands On e produzir o que era esperado naquele dia.

### A JORNADA

Ao final do _*Hands On*_ o arquivo distribuído com o app de to-do criado pela equipe do _THF-MOBILE_ serviu de modelo para este.

_**Desafio 1 - Rest Protheus**_

Para resolver este primeiro item a abordagem foi criar uma tabela personalizada SZ0 com 3 campos: Código (Z0_CODE), Conteúdo (Z0_CONTENT) e Data (Z0_DATE).

A escrita do rest para as operações de GET, POST, PUT e DELETE tiveram seus inúmeros problemas, os que eu lembro agora foram:

- Entender a necessidade de `prepare in ` / `rpcsetenv`, parentemente desde que o .ini esteja definido e no Header esteja definido o `"TenantId": "99,01"` o ambiente será inicializado na empresa correta;
- Identificar corretamente os conteúdos recebidos por parâmetros em cada operação do Rest;
- Formatar devidamente o retorno sem quebrar com caracteres não aceitos no UTF-8.

Os arquivos resultado deste processo estão na pasta:
```bash
$ /advpl/
$ /advpl/todomvc.prw
$ /advpl/todorest.prw
```

Uma opção para não passar pelos problemas mencionados seria publicar o rest deste cadastro MVC (o que pode levar a encontrar outras situações em que seria necessário ajustar ou intervir).

_**Desafio 2 - THF-MOBILE**_

Entender como o _**thf-mobile**_ funciona depois de ter sofrido um pouco em outro projeto exclusivamente com o `ionic` foi relativamente simples (especialmente por que esta etapa foi basicamente de `copy e paste` do app original cedido ao final do Hands On).

O único problema que consigo lembrar desse trecho foi a formatação da p* da data que exibida e lida na tela de edição/exibição de um to-do.
Quando estava olhando o app base não entendi a necessidade de 3 datas e depois percebi que a data só é devidamente carregada quando é uma string no formato `yyyy-mm-dd` senão será exibido conteúdo em branco.
A chave para resolver esta situação foram 3 atributos para data na classe `Task` e um método `format` para atribuir o conteúdo corretamente formatado nos 3 atributos.

Os arquivos envolvidos neste tratamento são:
```
$ /src/app/models/task.model.ts
$ /src/app/pages/add/add.ts
```


_**Os Testes**_

Esta foi a etapa que mais me fez perceber a importância de como quem oferece um serviço e quem o consumo precisam estar muito alinhados. Eu cometi a asneira de criar um envelope `"todo": "dados"` enquanto desenvolvia o rest no protheus e esqueci de olhar se era isso o esperado pelo modelo do app.

Descobri que tinha feito asneira só no teste e portanto tive que alterar isso em todos os métodos que eu tinha desenvolvido (por sorte só uma classe).

Afora esta situação com o formato esperado de `json` para o serviço do rest nada fez com que ficasse estagnado ou gerou problemas.

O único detalhe é uma situação percebida que não consegui entender o por que do erro ser apresentado no `console`.

 ```
 GET http://localhost:8084/rest/todo/ 504 (Gateway timeout)

 XMLHttpRequest cannot load http://localhost:8084/rest/todo/. No 'Access-Control-Allow-Origin' header is present on the requested resource. Origin 'http://localhost:8100' is therefore not allowed access. The response had HTTP status code 504.
 ```
 
 Aparentamente o problema é de [CORS](https://en.wikipedia.org/wiki/Cross-origin_resource_sharing) mas percebi que só deu esse erro quando duas requisições são feitas uma logo em seguida da outra. Talvez só por causa do ambiente ser de teste o problema tenha acontecido.


